---
author: Louis.bachetti
title: "Fantastic Bits"
date: 2023-05-11T21:33:35+02:00
description: "Developing an AI to play Quidditch ?"
weight: 1
---
### What is the Fantastic Bits Challenge ?
This challenge is developed by Codingame, you can find it here: [Fantastic Bits](https://www.example.com)

If you aren't used to Codingame challenge, I recommend you to take a look at their platform. 

For this challenge, the goal is pretty easy. We have two wizards that are playing Quidditch. We control both of them, and we have to score more goals than our opponent. Furthermore, we can move, shoot and cast spells. There is also Bludger that will block our wizard or the balls and with which we can also interact. 

We won't use every rule at the beginning, so we will go step by step. For this challenge I really want to keep things easy, understandable and adding a piece of functionality at the right moment. So the plan for this is: 
1. Designing the game
2. Movement and shoot
3. Casting spells 
4. Interacting with opponent's snaffles
5. Optimizing my strategy

Why this plan ? I'm more comfortable controlling everything I can before starting to interact with my opponents. Also, the spells could be a little bit tricky, so I thought that if my game is well-designed and my wizards already know how to move it will be easier to work with the spell. 
 

### Designing the game

As I'm working in C# on this challenge, I want to represent everything as an object. We have several items to create:
- Wizards, we have four wizard to create 2 by player. Ally and enemy wizard will be the same object. 
- Balls, from three to five, depending on the game. We will create one object and manage them into a list. 
- Bludgers, also a random amount, we will process them in the same way as the balls.
- Goals, I will create an object for them because I need to position my goal and enemy goal at the right place as I don't know in which side I am playing. 


You could think, at this point, that there is still one category that I didn't describe on my project, spells. We can off course see them as object with spell cost, name, usability. But I'm not sure how to handle them for now. 

Let's start with Wizards: 

    public class Wizard{

        // Position X
        public int Pos_x;

        // Position Y
        public int Pos_y;

        // If it's true, my wizard is holding a snaffle
        public bool Snaffle;

        // Mana point of the wizard
        public int mana;

        // Id of the wizard
        public int Id;

        // Velocity X 
        public int Vx;

        // Velocity Y
        public int Vy;

        // Constructor
        public Wizard(int x, int y, int snaffle, int entityId, int vx, int vy)
        {
            Pos_x = x;
            Pos_y = y;
            Snaffle = Convert.ToBoolean(snaffle);
            Id = entityId;
            Vx = vx;
            Vy = vy;
        }
    }


For the snaffle: 

    public class Snaffle
    {
        // Position x
        public int Pos_x;

        // Position y
        public int Pos_y;

        // Id 
        public int Id;

        // true if hte snaffle isn't hook by wizard.
        public bool IsTook = false;

        // velocity x
        public int Vx;

        // velocity y
        public int Vy;

        // Constructor
        public Snaffle(int x, int y, int entityId, int vx, int vy)
        {
            Pos_x = x;
            Pos_y = y;
            Id = entityId;
            Vx = vx;
            Vy = vy;
        }
    }


For the bludger: 

    public class Bludger
    {
        public int Pos_x;

        public int Pos_y;

        public int Id;

        public Bludger(int x, int y, int id)
        {
            Pos_x = x;
            Pos_y = y;
            Id = id;
        }
    }


And now for the goal. It's not tricky and probably not the best way to handle it, but my idea is to take the team ID as parameters and create the right goal. Now that I'm writing this, I think that I should have added another class attribute in order to tell me if it's left or right goal or maybe mine or enemy goal. 

    public class Goal
    {
        // Position x
        public int Pos_x;

        // This position is the middle of the goal, always set to 3750 as the map never move.
        public int Pos_y;

        // Constructor that set the pos x regarding the id of the team.
        public Goal(int idTeam)
        {
            Pos_y = 3750;
            if (idTeam == 0)
            {
                Pos_x = 16000;
            }
            else
            {
                Pos_x = 0;
            }
        }
    }

For this contest we sometimes need to calculate area, or other mathematical things, for this I've created a Utils class, but I won't show it now. 

### Movement and shoot
On the previous part we have created every object, now we need to create our first methods. My idea is for now to create a simple base, each wizard will move to the closest snaffles and shoot it to the goal and repeat until we win or lose. As every object is created, it should be only few lines of code.

First, let's create wizard and snuffles:

    List<Wizard> wizards = new List<Wizard>();
    List<Snaffle> snaffles = new List<Snaffle>();

And in a loop that read all arguments: 

    inputs = Console.ReadLine().Split(' ');
    int entityId = int.Parse(inputs[0]); // entity identifier
    string entityType = inputs[1]; // "WIZARD", "OPPONENT_WIZARD" or "SNAFFLE" (or "BLUDGER" after first league)
    int x = int.Parse(inputs[2]); // position
    int y = int.Parse(inputs[3]); // position
    int vx = int.Parse(inputs[4]); // velocity
    int vy = int.Parse(inputs[5]); // velocity
    int state = int.Parse(inputs[6]); // 1 if the wizard is holding a Snaffle, 0 otherwise
    if (entityType == "WIZARD")
        wizards.Add(new Wizard(x, y, state, entityId, vx, vy));
    else if (entityType == "SNAFFLE")
        snaffles.Add(new Snaffle(x, y, state, entityId, vx, vy));


Now we want a method that indicate which is the closest snaffle and ask my wizard to move to this snaffle:


    // Will calculat the distance to each Snaffle
    // We take the snaffle list as ref because we wnt to modify the isTook parameters.  
    public Snaffle ClosestSnaffle(ref List<Snaffle> snaffles)
    {
        int tot = Pos_x + Pos_y;
        double temp = 1000000;
        Snaffle choosen = snaffles.First();

        foreach (Snaffle item in snaffles)
        {
            var dist = Math.Sqrt(((Pos_x - item.Pos_x) * (Pos_x - item.Pos_x)) + ((Pos_y - item.Pos_y) * (Pos_y - item.Pos_y)));
            if (temp > dist && item.IsTook != true)
            {
                temp = dist;
                choosen = item;
                item.IsTook = true;
            }
        }
        return choosen;
    }

By copying and pasting this, I'm figuring out that the 'temp' variable is a terrible idea. Some way to optimizing, using null, using a dictionary, ordering the list with Linq... So many clean way and I choose the dirty one. But let's keep it for now. 

So we have the chosen one, now we just need to add one line for asking the wizard to move to it.


    Snaffle snaffleToGo = wiz.ClosestSnaffle(ref snaffles);
    Console.WriteLine($"MOVE {snaffleToGo.Pos_x + snaffleToGo.Vx} {snaffleToGo.Pos_y+ snaffleToGo.Vy} 150");


Maye you've noticed the small tricks here. If I only put pos_X and pos_Y my wizzard will go to the 'last known location'. By adding the actual velocity of the snaffle we will go to the real location. The 150 is the Wizard speed. I don't know what to do about it for now. 

Ok, my wizard can grab a snaffle now, we still need to throw it. We have a variable that telling us if our wizard is holding a snaffle so let's using it. 


    if (wiz.Snaffle)
        Console.WriteLine($"THROW {ennemyGoal.Pos_x} {ennemyGoal.Pos_y} 500");

### My first spell ! 
The first spell that I want to use is the offensive one. I think that scoring a goal as fast as possible is a better strategy than playing in defense. 

The spell is "Flippendo" and allow us to shoot on a snaffle from everywhere. This spell will shoot the snaffle straight away. I always want to shoot for the goal, I don't want to lose some magic for nothing.

In order to know if my shoot will be a goal or not, I have two options. The first one is to create a line passing through my wizard and the snaffle, if the line touch the goal then it's good!
My second idea is to create a triangle with the goal and my wizard and see if the snaffle is contained on it. I will implement this one. 

First let's create the triangle: 

    public static class Util
    {
        public static bool isInside(int x1, int y1, int x2, int y2, int x3, int y3, int x, int y)
        {
            /* Calculate area of triangle ABC */
            float A = area(x1, y1, x2, y2, x3, y3);

            /* Calculate area of triangle PBC */
            float A1 = area(x, y, x2, y2, x3, y3);

            /* Calculate area of triangle PAC */
            float A2 = area(x1, y1, x, y, x3, y3);

            /* Calculate area of triangle PAB */
            float A3 = area(x1, y1, x2, y2, x, y);

            /* Check if sum of A1, A2 and A3 is same as A */
            return (A == A1 + A2 + A3);
        }

        public static float area(int x1, int y1, int x2, int y2, int x3, int y3)
        {
            return (float)Math.Abs((x1 * (y2 - y3) + x2 * (y3 - y1) + x3 * (y1 - y2)) / 2.0);
        }
    }

It can seems messy to use x, y, A, as parameters but I use those methods for other contest so I keep them on my gitlab as generic as possible. 

And now we can use this method in our Wizard class with the snaffles list, the goal and the wizard position as parameters.

    public bool KnowIfAlignGoalOne(Snaffle snaffle, Goal goal)
    {
        if (Util.isInside(Pos_x, Pos_y, goal.Pos_x, (goal.Pos_y - 1800), goal.Pos_x, (goal.Pos_y + 1800), snaffle.Pos_x, snaffle.Pos_y))
            return true;
        else
            return false;
    }

And we implement this feature in our 'thinking method', keep it easy, for now everytime my AI think there is a possible goal then I use flippendo. 

    Snaffle snaffleAlignGoal = wiz.KnowIfAlignGoal(ref snaffles, myGoal);
    else if (myMagic >= 20 && snaffleAlignGoal != null)
        Console.WriteLine($"FLIPENDO {snaffleAlignGoal.Id}");

If we have on snaffle aligned with ennemy goal and enough magic then we shoot!

### Interacting with opponent's snaffles

At this point, I have two improvement in mind. We have two spells, Accio and Petrificus. The first one, attract a snaffle to our wizard and the other one, stop a wizard, a bludger or a snaffle. 

My two ideas are: 
- If a snaffle is too close from my goal, I use Petrificus to stop it.
- If a wizard can't score with any snaffle, then he will use Accio.

The easiest part, is to use Accio. Even if this implementation is easy, it's also a little bit messy and I should work again on that. 
I'm using the previous method to know if there is a snaffle that is aligned with the goal. If there is one, then I won't use Accio this turn and I will keep some magic for a Flipendo or a Petrificus. 

So we only have to add those lines:

    else if (myMagic >= 15 && snaffleAlignGoal != null)
        Console.WriteLine($"ACCIO {snaffleAlignGoal.Id}");

Now for the petrificus, we need a method that use a snaffles list as a parameters and calculate if we need to stop one or not. 

    public Snaffle snaffleTooClose(List<Snaffle> snaffles)
    {
        string side = Pos_x == 0 ? "L" : "R";
        if (side == "L")
        {
            foreach (Snaffle snaff in snaffles)
            {
                if (snaff.Pos_x < 1900 && (snaff.Pos_y > 1900 && snaff.Pos_y < 5480))
                    return snaff;
            }
        }
        if (side == "R")
        {
            foreach (Snaffle snaff in snaffles)
            {
                if (snaff.Pos_x > 14000 && (snaff.Pos_y > 1900 && snaff.Pos_y < 5480))
                    return snaff;
            }
        }
        return null;
    }


In order to do this, I simply define an area around the goal and check if a snaffle is inside or not. 
Looking back at this method, I should definitely add the snaffle velocity as parameters for the calculation... 

And I add it to the main loop:

    else if (toStop != null && myMagic >= 10)
        Console.WriteLine($"PETRIFICUS {toStop.Id}");


Finally, my main loop looks like this: 
    - Can I score ?
    - Do I have a snaffle that could be scored in some frame ? 
    - Does the enemy can score ? 
    - Otherwise, just move to the closest snaffle. 

### Conclusion

I'm writing this articles sometimes after participating in the contest. That was my first contest, and I've seen the code was sometimes messy and wasn't easily up for modification. In one week, I'm happy of what I did for a first contest. Learning how the IDE was working, and how to create a small AI looks like, gave me some trouble. That's why I wanted to keep the things really easy and just test a small adjustment and feature was enough. I almost climb to gold league with this code and that's a nice start for the next challenge in my point of view. 