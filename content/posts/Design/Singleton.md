---
title: "Singleton"
date: 2023-05-22T14:04:53+02:00
draft: true
---

### What's a Singleton ? 

A Singleton is a design pattern that will allow you to only create one instance of an Object. 

Why would you do that ? For some purpose, you want to have the exact same behavior for an object in your whole application, you could also not want an user of your library to instanciate his own object. For example a connection to a DataBase that you want to protect from bad usage, or an object in which you want to prevent multithreading malfunctions. 

It does not mean that you won't be able to call your object more than one time. It means that the object you are going to call is the same for your whole application and every change or parameters will be saved. 

### How to create a Singleton (Thread unsafe) ? 
In this example our class is called 'Singleton'. We only have one attribute, '_name', this attribute is here only for testing it at the end. 

In order to create a Singleton we need to hide the Constructor. If you let the constructor open then anybody will be able to instanciate your object. 
For doing this you just need to set the constructor Private. 

```C#
private Singleton(string name){
    _name = name;
}
```

Then we need to create a method that will create the instance of our object or that will return us the instance previously created if their is one. 
As we need to be able to call this method without instanciate the object we need to declare it as Static. We also need to store the instance as an attribute, for this we will have an attribute _instance.

The attribute is:

```C#
private static Singleton() _instance;
```

Then we need to implement GetInstance():

```C#
public static GetInstance(string name){
    if(_instance == null)
        _instance = new Singleton(name);
    return _instance;
}
```

Now we need to add some business logique inside of the class. You don't need to tag them as 'Static' as you will use the instance given by the method GetIstance(). 

Now if we create the whole class it look like: 

```C#
// This class is Sealed to prevent the inheritance. Singleton shouldn't be inherited.
public sealed Singleton(){

    // Our test attributes.
    private string _name

    // The instance
    private static Singleton _instance; 

    // The hidden (private) constructor
    private Singleton(string name){
        _name = name;
    }

    // Our visible instance constructor
    public static GetInstance(string name){
        if(_instance == null)
            _instance = new Singleton(name);
        return _instance;
    }
}
```

And here you have your first singleton !

But there is still one problem. If we are in a multithread environment it won't work. 
The method 'GetInstance' could be called twice at the same time by two different thread. As we don't have yet an instance, then we will create two instance instead of one and that will result as an error. 

### How to create a Singleton (Thread safe) ? 

In order to create a thread safe implementation of the singleton, you need to lock the creation of the new instance from other threads. 

For this we will use the object 'Lock'. That will only let the first thread to go into this condition. 
First we need to instanciate an object Lock as Class attribute. We set it as static for the same reason than previously. We won't need to do any modification on it so we also set it as 'Readonly'.

```C#
private static readonly object _lock = new object();
```

And then we use this object into our GetInstance() method: 

```C#
public static GetInstance(string name){
    if(_instance == null)
        lock(_lock)
            if(_instance == null)
                _instance = new Singleton(name);
        _instance = new Singleton(name);
    return _instance;
}
```

Why are we doing twice the condition ? Because we don't want to lock our thread (and having performance issue) after having the instance created. But at the same time, the condition is needed after the lock otherwise we could keep facing thread issue. 

Finally here is our thread safe singleton: 
 
```C#
// This class is Sealed to prevent the inheritance. Singleton shouldn't be inherited.
public sealed Singleton(){

    // Our test attributes.
    private string _name

    // The instance
    private static Singleton _instance; 

    // Thread locker
    private static readonly object _lock = new object();


    // The hidden (private) constructor
    private Singleton(string name){
        _name = name;
    }

    // Thread safe Instanciator
    public static GetInstance(string name){
    if(_instance == null)
        lock(_lock)
            if(_instance == null)
                _instance = new Singleton(name);
        _instance = new Singleton(name);
    return _instance;
    }
}
```